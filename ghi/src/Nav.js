import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark">
        <div className="container-fluid">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/appointments">
                        <img src={require('./images/VisCareTestLogo.png')} alt="" width="30" height="24" className="d-inline-block align-text-top m-2"/>
                        VisCare
                    </Link>
                </div>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Accounts
                </Link>
                <ul className="dropdown-menu">
                    <li>
                        <NavLink className="navbar-brand p-1" to="/accounts/list">Account List</NavLink>
                    </li>
                    <li>
                        <NavLink className="navbar-brand p-1" to="/accounts/update">Update Account</NavLink>
                    </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
                </Link>
                <ul className="dropdown-menu">
                    <li>
                        <NavLink className="navbar-brand p-1" to="/appointments">Appointment List</NavLink>
                    </li>
                    <li>
                        <NavLink className="navbar-brand p-1" to="/appointments/update">Update Appointment</NavLink>
                    </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Locations
                </Link>
                <ul className="dropdown-menu">
                    <li>
                        <NavLink className="navbar-brand p-1" to="locations">Locations</NavLink>
                    </li>
                    <li>
                        <NavLink className="navbar-brand p-1" to="locations/new">Add a Location</NavLink>
                    </li>
                    <li>
                        <NavLink className="navbar-brand p-1" to="locations/update/:id">Update a Location</NavLink>
                    </li>
                </ul>
            </li>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                </ul>
            </div>
            </ul>
        </div>
        </nav>
    )
}

export default Nav;
