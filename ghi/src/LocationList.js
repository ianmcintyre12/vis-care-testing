import { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

function LocationList(props) {
  const [locations, setLocations] = useState([]);
  const {token} = useToken();

  const getData = async () => {
    console.log(token)
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/locations`, {
      method: "GET",
      body: JSON.stringify(getData),
      credentials: "include",
      headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
    });
    if (response.ok) {
      const data = await response.json();
      setLocations(data);
    }
  };

  const handleDelete = async (id) => {
    const url = `${process.env.REACT_APP_API_HOST}/locations/${id}/`;
    // const fetchConfig = {
    //   method: "DELETE",
    //   headers: { "Content-Type": "application/json" },
    // };
    await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    getData();
  };

  useEffect(() => {
    if (token) {
      getData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[token]);

  if (locations) {
    return (
      <>
        <div className="container text-center">
          <h1 className="mb-2">Locations</h1>
        </div>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {locations.map((location) => {
                const key = location.id;
                return (
                  <tr key={key}>
                    <td>{location.id}</td>
                    <td>{location.name}</td>
                    <td>
                      <button
                        type="button"
                        className="btn btn-outline-danger"
                        onClick={() =>
                          handleDelete(location.id, props.getlocation)
                        }
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  } else {
    return (
      <div className="container text-center">
        <h1>No Locations available</h1>
      </div>
    );
  }
}

export default LocationList;
