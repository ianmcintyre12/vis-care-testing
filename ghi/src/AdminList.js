import {useEffect, useState} from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./style.css";

function AdminList() {
    const { token } = useToken();
    const [admin, setAdmin] = useState([]);

        const getData = async () => {
            const url = await fetch(`${process.env.REACT_APP_API_HOST}/admin`);

            const response = await fetch(url, {
                method: "get",
                body: JSON.stringify(getData),
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                },
            });
            if (response.ok) {
                const data = await response.json();
                setAdmin(data)
            }
        };

        useEffect(() => {
            getData()
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [token]);
// what is in the array causes use effect to fire. you log in token updates and token changes so it get the data
        return (
            <div>
                <h1>Admin List</h1>
                {Array.isArray(admin) ?
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Usernames</th>
                        </tr>
                    </thead>
                    <tbody>
                        {admin.map (admin => {
                            return (
                                <tr key={admin.id}>
                                    <td>{admin.username}</td>
                                </tr>
                            )})
                        }
                    </tbody>
                </table>
                    : <p>You must be logged in to view this page.</p>
                }
            </div>


            // <div>
            // <h5>Admin List</h5>
            //     <div className="col d-flex justify-content-center">
            //         <div className="card m-2 p-2">
            //             <img src="https://trinitymedgroup.com/wp-content/uploads/AdobeStock_270629805-e1569338213607.jpeg" className="card-img-top" alt="..."/>
            //             <div className="card-body">
            //             </div>
            //             <ul className="list-group list-group-flush">
            //                 <li className="list-group-item">An item</li>
            //             </ul>
            //         </div>
            //     </div>
            // </div>
        )
}

export default AdminList
