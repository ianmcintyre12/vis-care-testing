import { useEffect, useState } from "react";
import { NavLink, useNavigate } from 'react-router-dom';
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function AppointmentList() {
  const { token } = useToken();
  const navigate = useNavigate();
  const [appointments, setAppointments] = useState([]);

  async function getAppointments(token) {
    const url = `${process.env.REACT_APP_API_HOST}/appointments`;
    const response = await fetch(url, {
      headers: { Authorization: `Bearer ${token}` },
      method: "GET"
    });
    if (response.ok) {
      const data = await response.json();
      setAppointments(data);
    }
  }

  useEffect(() => {
    if (!(token)) {
      navigate("/login");
    }
    getAppointments(token);
  }, [token, navigate])

  const handleDelete = async (e) => {
    e.preventDefault();
    const id = e.target.value;
    const url = `${process.env.REACT_APP_API_HOST}/appointments/${id}/`;
    await fetch(url, {
      headers: { Authorization: `Bearer ${token}` },
      method: "DELETE"
    });
    getAppointments(token);
  };

  const handleUpdate = id => {
    navigate(`/appointments/update/${id}`)
  }


  return (
    <>
      <div className="row text-center mt-3">
        <h1 className="mb-2">Appointments</h1>
        <p><NavLink to="/appointments/new" className="btn btn-sm btn-primary">Schedule New Appointment</NavLink></p>
      </div>
      <div className="row justify-content-center text-center">
        <div className='col'>
          <table className='table table-striped table-hover'>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Group Size</th>
                <th>Date</th>
                <th>Time</th>
                <th>Location</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {appointments.map(appointment => {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.id}</td>
                    <td>{appointment.visitor_name}</td>
                    <td>{appointment.group_size}</td>
                    <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                    <td>{new Date(appointment.date_time).toLocaleTimeString(undefined, { hour: "numeric", minute: "numeric" })}</td>
                    <td>{appointment.location}</td>
                    <td>{appointment.phone_number}</td>
                    <td>{appointment.email}</td>
                    <td>
                      <button className="btn btn-info mx-2" onClick={() => handleUpdate(appointment.id)} name='update'>Update</button>
                      <button className="btn btn-danger mx-2" onClick={handleDelete} value={appointment.id} name='cancel'>Cancel</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
