import { useState } from "react";

function CreateAccount() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsernameChange = (event) => {
        const value = event.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (event) => {
        const value = event.target.value;
        setPassword(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            username,
            password,
        };

        const adminUrl = `${process.env.REACT_APP_API_HOST}/admin/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(adminUrl, fetchConfig);
        if (response.ok) {
            setUsername('');
            setPassword('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an account</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleUsernameChange} placeholder="username" required type="text" name="username" id="username" value={username} className="form-control" />
                            <label htmlFor="username">Username: </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePasswordChange} placeholder="password" required type="text" name="password" id="password" value={password} className="form-control" />
                            <label htmlFor="password">Password:</label>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateAccount
