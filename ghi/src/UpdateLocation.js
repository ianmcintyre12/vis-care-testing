import React, { useState } from "react";

function UpdateLocationForm() {
  const [formData, setFormData] = useState({
    name: "",
    status: "",
  });

  const handleSubmit = async (event) => {
    event.prevventDefault();
    const url = "localhost somewhere";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${token}`
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        status: "",
      });
    }
  };
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Update a location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Location code"
                required
                type="text"
                name="identifier"
                id="identifier"
                className="form-control"
              />
              <label htmlFor="identifier">**DROPDOWN OF EXISTING LOCATIONS OR VIEW SPECIFIC and cannot update this field**</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="Name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                placeholder="Status"
                required
                name="status"
                id="status"
                className="form-control"
              >
                <option value="">Select Status</option>
                <option value="active">Active</option>
                <option value="suspended">Suspended</option>
                <option value="inactive">Inactive</option>
              </select>
              <label htmlFor="status">Status</label>
            </div>
            <button className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default UpdateLocationForm;
