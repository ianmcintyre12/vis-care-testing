import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";


function VisitForm() {
  const [formData, setFormData] = useState({
    visitor_name: "",
    phone_number: "",
    email: "",
    date_time: "",
    location: "",
    group_size: "",
  });
  const [locations, setLocations] = useState([]);

  const { token } = useToken();

  const getData = async () => {
    console.log(token)
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/locations`, {
      method: "GET",
      body: JSON.stringify(getData),
      credentials: "include",
      headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
    });
    if (response.ok) {
      const data = await response.json();
      setLocations(data);
    }
  };

  useEffect(() => {
    if (token) {
      getData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);


  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };


  const handleDateChange = (event) => {
    const date = new Date(event.target.value);
    const formattedDate = date.toISOString().slice(0, 16);
    setFormData(prevState => ({
      ...prevState,
      date_time: formattedDate,
    }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const appointmentURL = `${process.env.REACT_APP_API_HOST}/appointments/`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
    };
    try {
      const response = await fetch(appointmentURL, fetchConfig);
      if (response.ok) {
        setFormData({
          visitor_name: "",
          phone_number: "",
          email: "",
          date_time: "",
          location: "",
          group_size: "",
        });
      } else {
        console.error("Error submitting form:", response.statusText);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Schedule a Visit</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                value={formData.visitor_name}
                onChange={handleChange}
                placeholder="First & Last Name"
                required
                type="text"
                name="visitor_name"
                id="visitor_name"
                className="form-control"
              />
              <label htmlFor="visitor_name"> Primary Visitor Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.date_time}
                onChange={handleDateChange}
                placeholder="yyyy-MM-ddThh:mm"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time"> Date & Time</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.group_size}
                onChange={handleChange}
                placeholder="##"
                required
                type="text"
                name="group_size"
                id="group_size"
                className="form-control"
              />
              <label htmlFor="group_size"> Group Size</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.phone_number}
                onChange={handleChange}
                placeholder="(xxx)xxx-xxxx"
                required
                type="text"
                name="phone_number"
                id="phone_number"
                className="form-control"
              />
              <label htmlFor="phone_number">
                {" "}
                Primary Visitor Phone Number
              </label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.email}
                onChange={handleChange}
                placeholder="example@visitor.com"
                required
                type="text"
                name="email"
                id="email"
                className="form-control"
              />
              <label htmlFor="email">
                {" "}
                Primary Visitor E-mail Address
              </label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={formData.location}
                onChange={handleChange}
                required
                name="location"
                id="location"
                className="form-control"
              >
                <option value="">Select Location</option>
                {locations.map(location => (
                  <option key={location.id} value={location.id}>
                    {location.name}
                  </option>
                ))}
              </select>
              <label htmlFor="location"> Location</label>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VisitForm;
