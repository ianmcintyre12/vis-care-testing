import { NavLink } from 'react-router-dom';

function HomePage() {
    console.log('we home')
    return (
        <div className="card mb-3 m-5 mx-auto" style={{width:'700px'}}>
            <div className="row no-gutters">
                <div className="col-md-6">
                <img src="https://image1.masterfile.com/getImage/NjAwLTAxOTkyOTgzZW4uMDAwMDAwMDA=AA6ak4/600-01992983en_Masterfile.jpg" className="card-img " alt="..."/>
                </div>
                <div className="col-md-6">
                    <div className="card-body mx-auto">
                            <h5 className="card-title">Welcome to VisCare</h5>
                            <div className="p-2">
                                <p>If you already have an admin account please log in</p>
                                <p><NavLink to="login" className="btn btn-info">Login</NavLink></p>
                            </div>
                            <div className="p-2">
                                <p>If you need an admin account please create one</p>
                                <p><NavLink to="accounts/create" className="btn btn-info">Create Admin Account</NavLink></p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage;
