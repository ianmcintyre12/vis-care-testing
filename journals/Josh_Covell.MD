## Journals

1/8 - 1/12

- Developed idea for app (VisCare)
- Completed wireframes and API endpoints with the team this week.
- Got confused about direciton of project. After talking with Alex and Paul, decided to keep it simple to start and expand it as we start to build it out.
- Great team and great progress for the week. Looking forward to next week!

1/12 - 1/22

- created create account endpoint together as a team
- individually created rest of endpoints. I made the update location endpoint
- began backend auth

1/22 - 1/30

- finished backend auth with team
- individually started protecting endpoints on backend
- started on some frontend components
- added browser router and bootstrap to start organizing frontend
- created login page to log in a user and receive a token
- started on frontend auth
