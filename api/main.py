from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts
from routers import appointments
from routers import locations
from authenticator import authenticator

app = FastAPI()
app.include_router(accounts.router)
app.include_router(appointments.router)
app.include_router(locations.router)
app.include_router(authenticator.router)
cors = os.environ["CORS_HOST"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        cors
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }
