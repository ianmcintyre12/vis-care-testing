import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AdminRepository, AdminOutWithPassword


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: AdminRepository,
    ):
        # Use your repo to get the account based on the
        # username (which could be an email)
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: AdminRepository = Depends(),
    ):
        # Return the accounts. That's it.
        return accounts

    def get_hashed_password(self, account: AdminOutWithPassword):
        # Return the encrypted password value from your
        # account object
        print(account)
        return account.__getitem__('hashed_password')

    def get_account_data_for_cookie(self, account: AdminOutWithPassword):
        # Return the username and the data for the cookie.
        # You must return TWO values from this method.
        return account["username"], AdminOutWithPassword(**account)


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
