from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List

# this file has nothing to do with the database. these are for our ENDPOINTS


class Error(BaseModel):
    message: str


class LocationIn(BaseModel):
    name: str


class LocationOut(BaseModel):
    id: int
    name: str


class LocationRepository(BaseModel):
    def create(self, location: LocationIn) -> LocationOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO locations
                            (name)
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [
                            location.name,
                        ]
                    )
                    id = result.fetchone()[0]
                    return self.location_in_to_out(id, location)
        except Exception as e:
            print(e)
            return {"message": "Could not get all locations"}

    def get_one(self, location_id: int) -> Optional[LocationOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                        FROM locations
                        WHERE id = %s
                        """,
                        [location_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_location_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that location"}

    def get_all(self) -> Union[Error, List[LocationOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name
                        FROM locations
                        ORDER BY name;
                        """
                    )
                    # result= []
                    # for record in db:
                    #     location= LocationOut(
                    #         id= record[0],
                    #         name= record[1],
                    #         from_date= record[2],
                    #         to_date= record[3],
                    #         thoughts= record[4],
                    #     )
                    #     result.append(location)
                    # return result

                    return [
                        LocationOut(
                            id=record[0],
                            name=record[1],
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all locations"}

    def update_location(
            self,
            id: int,
            location: LocationIn
    ) -> Union[LocationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE locations
                        SET name = %s
                        WHERE id =%s
                        RETURNING *
                        """,
                        [location.name, id]
                    )

                    old_data = location.dict()
                    if db.fetchone():
                        return LocationOut(id=id, **old_data)
                    return {"message": "could not update location"}
        except Exception:
            return {"message: could not update location"}

    def delete(self, location_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM locations
                        WHERE id = %s
                        """,
                        [location_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def location_in_to_out(self, id: int, location: LocationIn):
        old_data = location.dict()
        return LocationOut(id=id, **old_data)

    def record_to_location_out(self, record):
        return LocationOut(
            id=record[0],
            name=record[1],
        )
