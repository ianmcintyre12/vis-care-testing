from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.locations import LocationRepository, LocationOut
from queries.accounts import AdminOut


client = TestClient(app)


class MockCreateLocation:
    def create(self, location):
        return {"id": 1, "name": location.name}


class MockLocationList:
    def get_all(self):
        return [LocationOut(id=1, name="The West Wing")]


class MockDeleteLocation:
    def delete(self, location_id):
        return True


def fake_get_current_account_data():
    return AdminOut(id=1, username="TEST")


def test_get_locations():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[LocationRepository] = MockLocationList
    expected_response = [{"id": 1, "name": "The West Wing"}]
    # Act
    response = client.get("/locations")
    # Clean up
    app.dependency_overrides = {}
    # Assert
    assert response.status_code == 200
    assert response.json() == expected_response


def test_create_location():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[LocationRepository] = MockCreateLocation
    location = {
        "name": "The West Wing"
    }
    expected = {
        "id": 1,
        "name": "The West Wing"
    }
    # Act
    response = client.post("/locations", json=location)
    # Clean up
    app.dependency_overrides = {}
    # Assert
    assert response.status_code == 200
    assert response.json() == expected


def test_delete_location():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[LocationRepository] = MockDeleteLocation
    location_id_to_delete = 1
    # Act
    response = client.delete(f"/locations/{location_id_to_delete}")
    # Clean up
    app.dependency_overrides = {}
    # Assert
    assert response.status_code == 200
    assert response.json() is True
