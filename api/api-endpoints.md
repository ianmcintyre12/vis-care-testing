### Appointment List

- Endpoint path: /appointments/
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of appointments
- Response shape:
  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "status": Pending/Approved/Rejected,
          "location": string,
          "phone_number": int,
          "email": string
      }
    ]
  }
  ```

### Create Appointment

- Endpoint path: /appointments/
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": int,
          "email": string,
          "status": Pending/Approved/Rejected,
      }
    ]
  }
  ```

- Response: Creation of new appointment
- Response shape:
  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": int,
          "email": string,
          "status": "Pending"
      }
    ]
  }
  ```

### Update Appointment

- Endpoint path: /appointment/<int:id>/
- Endpoint method: PUT

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": int,
          "email": string,
          "status": Pending/Approved/Rejected,
      }
    ]
  }
  ```

- Response: Return all appointment details including what was updated
- Response shape (JSON):
  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": int,
          "email": string,
          "status": Pending/Approved/Rejected,
      }
    ]
  }
  ```

### Delete Appointment

- Endpoint path: /appointment/<int:id>/
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Message relaying deletion of appointment

- Response shape (JSON):
  ```json
  "Appointment successfully deleted."
  ```

### Admin List

- Endpoint path: /admin/
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of admins
- Response shape:
  ```json
  {
    "admin": [
      {
          "username": string,
      }
    ]
  }
  ```

### Create Admin

- Endpoint path: /admin/
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:
  ```json
  {
    "admin": [
      {
          "username": string,
          "password": string,
      }
    ]
  }
  ```

### Update Admin

- Endpoint path: /admin/<int:id>/
- Endpoint method: PUT

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  {
    "admin": [
      {
          "password": string,
      }
    ]
  }
  ```

- Response: Upon creation of admin account they are redirected to the appointment list page
- Response shape (JSON):
  ```json
  {
    "admin": [
      {
          "username": string,
          "password": string,
      }
    ]
  }
  ```

### Delete admin

- Endpoint path: /admin/<int:id>/
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Potential pop up notification for deletion of account after redirect

- Response shape (JSON):
  ```json
  "Admin successfully deleted."
  ```

### Location List

- Endpoint path: /locations/
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: A list of locations in the hospital
- Response shape:
  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```

### Create locations

- Endpoint path: /locations/
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```

- Response: Creation of new locations
- Response shape:
  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```

### Update locations

- Endpoint path: /locations/<int:id>/
- Endpoint method: PUT

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```

- Response: Return all location details including what was updated
- Response shape (JSON):
  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```

### Delete locations

- Endpoint path: /locations/<int:id>/
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Potential pop up notification for deletion of account after redirect to list of locations

- Response shape (JSON):
  ```json
  "Locations successfully deleted."
  ```
