steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE locations (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(30) NOT NULL

        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE locations;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE appointments (
            id SERIAL PRIMARY KEY NOT NULL,
            visitor_name VARCHAR(50) NOT NULL,
            group_size INTEGER NOT NULL,
            date_time TIMESTAMP NOT NULL,
            location INTEGER,
            FOREIGN KEY (location) REFERENCES locations,
            phone_number VARCHAR(13) NOT NULL,
            email VARCHAR(50) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE appointments;
        """
    ]
]
