from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from authenticator import authenticator
from queries.locations import (
    Error,
    LocationIn,
    LocationRepository,
    LocationOut,
)


router = APIRouter()


@router.post("/locations", response_model=Union[LocationOut, Error])
def create_location(
    location: LocationIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: LocationRepository = Depends()
):
    return repo.create(location)


@router.get("/locations/{location_id}", response_model=Optional[LocationOut])
def get_one(
    location_id: int,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: LocationRepository = Depends(),
) -> LocationOut:
    location = repo.get_one(location_id)
    if location is None:
        response.status_code = 404
    return location


@router.get("/locations", response_model=Union[List[LocationOut], Error])
def get_all(
    repo: LocationRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all()


@router.put("/locations/{id}", response_model=Union[LocationOut, Error])
async def update_location(
        id: int,
        location: LocationIn,
        response: Response,
        repo: LocationRepository = Depends(),
        account_data: dict = Depends(authenticator.get_current_account_data)
) -> Union[LocationOut, Error]:
    update = repo.update_location(id, location)
    if type(update) is not LocationOut:
        response.status_code = 404
    return update


@router.delete("/locations/{id}", response_model=bool)
def delete_location(
    id: int,
    repo: LocationRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(id)
